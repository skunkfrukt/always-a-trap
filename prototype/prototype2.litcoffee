Verbs are the player's way of interacting with the world. There are two kinds of verbs, in a sense:
* Hand gesture verbs, which are initially available and can't be lost.
* Item verbs, which are obtained by picking up items.
	verbs =
		# Hand Gestures
		'rock':
			icon: 'graphics/v_rock.svg'
		'paper':
			icon: 'graphics/v_paper.svg'
		'scissors':
			icon: 'graphics/v_scissors.svg'
		'lizard':
			icon: 'graphics/v_lizard.svg'
		'claw':
			icon: 'graphics/v_claw.svg'
		'bird':
			icon: 'graphics/v_bird.svg'
			
		# Items
		'hammer':
			icon: 'graphics/v_hammer.svg'