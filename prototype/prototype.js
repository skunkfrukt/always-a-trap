"use strict";

/*  
 *  Game Element Definitions
 */

var verbs = {
	// Hand Gestures
	'rock': {'icon': 'graphics/v_rock.svg'},
	'paper': {'icon': 'graphics/v_paper.svg'},
	'scissors': {'icon': 'graphics/v_scissors.svg'},
	'lizard': {'icon': 'graphics/v_lizard.svg'},
	'claw': {'icon': 'graphics/v_claw.svg'},
	'bird': {'icon': 'graphics/v_bird.svg'},
	
	// Items
	'hammer': {'icon': 'graphics/v_hammer.svg'},
	'public-key': {'icon': 'graphics/v_public-key.svg'},
	'private-key': {'icon': 'graphics/v_private-key.svg'}
};

var MAX_INVENTORY = 12;
var INITIAL_VERBS = ['rock', 'paper', 'scissors', 'lizard', 'claw', 'bird'];

var lastClickedEle = null;

var inventory = [];

function addVerbToInventory(verbName) {
	if (!(verbName in verbs)) {
		alert("No such verb: " + verbName);
		return false;
	} else if (inventory.length >= MAX_INVENTORY) {
		alert("Inventory is full.");
		return false;
	} else {
		inventory.push(verbName);
		var ele = $('<img class=\"verbIcon\" src=\"' + verbs[verbName].icon + '\">');
		ele.data('verbName', verbName);
		$('#inventory').append(ele);
	}
}

var selectedVerb = null;

function selectVerbIcon(vIcon) {
	$('.verbIcon').removeClass('selected');
	if (vIcon) {
		vIcon.addClass('selected');
	}
}

var rooms = [];
var items = {
	"door": {
		"image": "graphics/p_frontdoor-closed.svg",
		"responses": {
			"rock": "Knock! Knock!",
			"bird": function() {alert('Calling response as function');},
			"claw": function() {transformThisItem("openDoor");}
		}
	},
	"knas": {
		"image": "graphics/v_bird.svg",
		"responses": {
			"bird": "Nu blev det knas."
		}
	},
	"openDoor": {
		"image": "graphics/p_frontdoor-open.svg",
		"responses": {"paper": function() {transformThisItem("door");}}
	},
	"wholeVase": {
		"image": "graphics/i_vase-whole.svg",
		"responses": {"rock": function() {transformThisItem("chippedVase");}}
	},
	"chippedVase": {
		"image": "graphics/i_vase-chipped.svg",
		"responses": {"rock": function() {transformThisItem("crackedVase");}}
	},
	"crackedVase": {
		"image": "graphics/i_vase-cracked.svg",
		"responses": {"rock": function() {transformThisItem("shatteredVase");}, "lizard": "Baboon jeepers wow inanimately overslept that ebulliently far far goodness awesome charmingly grimaced however arose much egret rebound the before oh some cost and mongoose underlay bawdily tentative cardinal masochistic lizard this and komodo versus far more more that and more off porcupine much gosh much yet belched stiffly jaguar far beside incorrect and bit scratched flexed beauteous some a some extraordinary amongst and much purposefully input spread and less fetchingly skimpy locked poetic less on crud pill some much thus husky lynx ignobly instead however lazy including esoteric tenable dear gosh far dear courageous this far strongly one or."}
	}
};

rooms.push({"bgImage": "graphics/r_entrance.svg", "items": [
	{"itemId": "door", "position": [120, 75], "scale": 1.0},
	{"itemId": "wholeVase", "position": [350, 220], "scale": 1.0}]});

function handleResponse(clickedItemEle) {
	lastClickedEle = clickedItemEle;
	var itemId = clickedItemEle.data('itemId')
	console.log('Handling response of ' + itemId + ' to ' + selectedVerb);
	var item = items[itemId];
	if (selectedVerb in item.responses) {
		var response = item.responses[selectedVerb];
		if (typeof response == "string") {
			$('#flavourText').empty();
			var fTextContents = $('<p>');
			fTextContents.html(response);
			$('#flavourText').append(fTextContents);
			showFlavourText();
			console.log(response);
		} else {
			response();
		}
	}
}

function transformThisItem(newItemId) {
	var oldItemEle = lastClickedEle;
	var pos = oldItemEle.position();
	var newItemEle = createItemEle(newItemId);
	oldItemEle.replaceWith(newItemEle);
	newItemEle.css('left', pos.left);
	newItemEle.css('top', pos.top);
}

function drawRoom(roomId) {
	var room = rooms[roomId];
	$('#roomView').empty();
	$('#roomView').css('background', 'url("' + room.bgImage + '")');
	for (var i = 0; i < room.items.length; i++) {
		var itemId = room.items[i].itemId;
		var pos = room.items[i].position;
		//var scale = room.items[i].scale;
		var itemEle = createItemEle(itemId);
		itemEle.css('left', pos[0]);
		itemEle.css('top', pos[1]);
		$('#roomView').append(itemEle);
	}
}

function createItemEle(itemId) {
	var item = items[itemId];
	var itemEle = $('<img src="' + item.image + '" class="roomItem">');
	itemEle.data('itemId', itemId);
	if (item.hasOwnProperty('responses')) {
		itemEle.click(function() {handleResponse($(this));});
	}
	return itemEle;
}

function showFlavourText() {
	$('#inventory').addClass('hidden');
	$('#flavourText').removeClass('hidden');
}

function showInventory() {
	$('#flavourText').addClass('hidden');
	$('#inventory').removeClass('hidden');
}

$(document).ready(
	function() {
		
		for (var i = 0; i < INITIAL_VERBS.length; i++) {
			addVerbToInventory(INITIAL_VERBS[i]);
		}
		$('.verbIcon').click(function() {
			var verbName = $(this).data('verbName')
			console.log('Clicked ' + verbName)
			if (verbName != selectedVerb) {
				console.log('Selected verb ' + verbName)
				selectVerbIcon($(this));
				selectedVerb = verbName;
				$('#roomDesc').text(verbName);
			} else {
				console.log('Cleared selected verb')
				selectVerbIcon(null);
				selectedVerb = null;
			}
		});
		$('#flavourText').click(showInventory);
		drawRoom(0);
	}
);