verbs =
	# Hand Gestures
	'rock':
		icon: 'graphics/v_rock.svg'
	'paper':
		icon: 'graphics/v_paper.svg'
	'scissors':
		icon: 'graphics/v_scissors.svg'
	'lizard':
		icon: 'graphics/v_lizard.svg'
	'claw':
		icon: 'graphics/v_claw.svg'
	'bird':
		icon: 'graphics/v_bird.svg'
		
	# Items
	'hammer':
		icon: 'graphics/v_hammer.svg'